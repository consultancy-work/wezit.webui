﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.WebUI.Data;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Helpers.General;
using Wezit.WebUI.Models;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Exceptions;
using System.Web;

namespace Wezit.WebUI.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly INotificationHelper _notificationHelper;

        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }
        public string CustomerUploads
        {
            get
            {
                return _configuration["CustomerUploads"];
            }
        }
        public string CompanyUploads
        {
            get
            {
                return _configuration["CompanyUploads"];
            }
        }

        public CustomerController(IConfiguration configuration,
            IRestClient restClient,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            INotificationHelper notificationHelper)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _restClient.UseNewtonsoftJson();
            _userManager = userManager;
            _signInManager = signInManager;
            _notificationHelper = notificationHelper;
        }
        // GET: CustomerController
        public async Task<IActionResult> Index()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest("Customers/GetList", DataFormat.Json);
            var customers = await _restClient.GetAsync<IEnumerable<CustomerDTO>>(request);
            if (customers == null)
            {
                customers = new List<CustomerDTO>();
            }

            return View(customers);
        }

        // GET: CustomerController/Details/
        public async Task<IActionResult> Details(int customerId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"Customers/GetSingle?id={customerId}", DataFormat.Json);
            var customer = await _restClient.GetAsync<CustomerDTO>(request);
            if (customer == null)
            {
                customer = new CustomerDTO();
            }

            return View(customer);
        }

        // GET: CustomerController/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            return View(new CustomerViewModel());
        }

        // POST: CustomerController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Create([Bind] CustomerViewModel customerViewModel, IFormFile nationalId, IFormFile companyRegCard)
        {
            var fileToUpload = customerViewModel.IsCompany ? companyRegCard : nationalId;
            if (fileToUpload is not null && fileToUpload.Length > 0)
            {
                var fileUploadResponse = await UploadFile(customerViewModel.IsCompany, fileToUpload);
                if (!fileUploadResponse.HasErrorOccured)
                {
                    CustomerDTO customer = customerViewModel;
                    customer.CustomerIdPath = $"{fileUploadResponse.Message}";
                    var request = new RestRequest("Customers/Create")
                        .AddJsonBody(customer);
                    var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

                    string jsonString = JsonConvert.SerializeObject(response.Data.Data);
                    var responseObject = JsonConvert.DeserializeObject<ReturnObject>(jsonString);
                    var newCustomer = responseObject.Entity;

                    if (response.StatusCode == HttpStatusCode.Created)
                    {
                        // AppContextHelper.SetToastMessage("Process complete, ", MessageType.Success, 1, Response);
                        var user = new ApplicationUser
                        {
                            UserName = customerViewModel.EmailAddress,
                            Email = customerViewModel.EmailAddress,
                            FirstName = customerViewModel.Firstname,
                            LastName = customerViewModel.Lastname,
                            GroupId = 3,
                            UserId = newCustomer.CustomerId
                        };

                        var result = await _userManager.CreateAsync(user, customerViewModel.Password);

                        if (result.Succeeded)
                        {
                            if (!string.IsNullOrEmpty(customer.EmailAddress))
                            {
                                var endPoint = Url.Action("VerifyEmail", "Account", new { ver = user.UserId.ToString() });
                                var url = $"http://{Request.Host}{endPoint}";

                                var subject = "Wezit Account Verification";
                                var body = "Thanks for signing up! Your account has been created, you can login with your credentials after you have activated your account by pressing the url below.<br />Please click this link to activate your account: " + url + " \r\n";

                                _notificationHelper.SendEmail(customer.EmailAddress, subject, body);

                                //await _signInManager.SignInAsync(user, isPersistent: false);
                                ModelState.AddModelError("", "Verify Your email address");
                            }
                            else
                            {
                                var ran = new Random();
                                var verificationCode = ran.Next(10000, 99999);
                                request = new RestRequest($"VerificationCodes/AddVerification?phoneNumber={customer.PhoneNumber}&code={verificationCode}", DataFormat.Json);
                                
                                var notificationResponse = _notificationHelper.SendSms(customer.EmailAddress, verificationCode.ToString());
                                
                                if (notificationResponse.HasErrorOccured)
                                {
                                    ModelState.AddModelError("", notificationResponse.Message);
                                }
                                
                                return RedirectToAction("VerifyPhone", "Account", new { pn = customer.PhoneNumber });
                            }

                            return RedirectToAction("Login", "Account");

                        }

                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError("", error.Description);
                        }

                    }
                    else
                    {
                        var responseMessage = response.Data.Message ?? response.ErrorMessage;
                        AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                        ModelState.AddModelError("", responseMessage);
                    }
                }
                else
                {
                    AppContextHelper.SetToastMessage(fileUploadResponse.Message, MessageType.Danger, 1, Response);
                    ModelState.AddModelError("", fileUploadResponse.Message);
                }
            }
            else
            {
                var errorMessage = customerViewModel.IsCompany ? "Upload your Company Certificate" : "Upload your national ID";
                AppContextHelper.SetToastMessage(errorMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", errorMessage);
            }

            return View(new CustomerViewModel());
        }

        // GET: CustomerController/Edit/
        public async Task<IActionResult> Edit(int customerId)
        {
            var request = new RestRequest($"Customers/GetSingle?id={customerId}", DataFormat.Json);
            var customer = await _restClient.GetAsync<CustomerDTO>(request);
            if (customer == null)
            {
                customer = new CustomerDTO();
            }

            return View(customer);
        }

        // POST: CustomerController/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int customerId, [Bind] CustomerDTO customerDTO)
        {
            var request = new RestRequest($"Customers/Update?id={customerId}")
                    .AddJsonBody(customerDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.PUT);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new CustomerDTO());
        }

        // GET: CustomerController/Delete/
        public async Task<IActionResult> Delete(int customerId)
        {
            var request = new RestRequest($"Customers/GetSingle?id={customerId}", DataFormat.Json);
            var customer = await _restClient.GetAsync<CustomerDTO>(request);
            if (customer == null)
            {
                customer = new CustomerDTO();
            }

            return View(customer);
        }

        // POST: CustomerController/Delete/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int customerId, IFormCollection collection)
        {
            var request = new RestRequest($"Customers/Delete?id={customerId}");
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("The item was deleted successfully 🚮", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new CustomerDTO());
        }
        public async Task<IActionResult> Download(string fileLocation)
        {
            //var rootFilePath = ;
            //var fullPath = Path.Combine(rootFilePath,path);
            //check if the file exist on the server
            if (!System.IO.File.Exists(fileLocation))
            {
                return NotFound($"This file does not exist on the server");
            }

            var memoryStream = new MemoryStream();
            using (var fileStream = new FileStream(fileLocation, FileMode.Open))
            {
                await fileStream.CopyToAsync(memoryStream);
            }
            memoryStream.Position = 0;
            return File(memoryStream, "application/octet-stream", $"customerFile.{fileLocation.Split(".")[1]}");
        }
        public async Task<FeedbackMessage> UploadFile(bool isCompany, IFormFile fileToUpload)
        {
            var validFileTypes = new List<string> { ".docx", ".png", ".jpg", ".jpeg", ".pdf" };
            var fileExtension = Path.GetExtension(fileToUpload.FileName);
            var fileName = DateTime.UtcNow.ToString("ddMMyyyyhhmmssf");
            var uploadPath = isCompany ? CompanyUploads : CustomerUploads;

            if (validFileTypes.Contains(fileExtension))
            {
                try
                {
                    using (var stream = System.IO.File.Create($"{uploadPath}{fileName}{fileExtension}"))
                    {
                        await fileToUpload.CopyToAsync(stream);
                    }
                }
                catch (Exception e)
                {
                    return new FeedbackMessage
                    {
                        HasErrorOccured = true,
                        Message = $"An error occured: {e.Message}"
                    };
                }
            }
            else
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = $"An error occured: invalid file type"
                };
            }


            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Message = $"{uploadPath}{fileName}{fileExtension}"
            };
        }
        public class ReturnObject
        {
            public CustomerDTO Entity { get; set; }
            public int State { get; set; }
        }
    }
}
