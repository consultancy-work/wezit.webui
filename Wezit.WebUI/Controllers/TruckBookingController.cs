﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.DataModels.ViewModels;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Helpers.General;
using Wezit.WebUI.Models;
using Wezit.WebUI.SimpleService;

namespace Wezit.WebUI.Controllers
{
    [Authorize]
    public class TruckBookingController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;
        private readonly IDataService _dataService;

        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }

        public TruckBookingController(IConfiguration configuration,
            IRestClient restClient,
            IDataService dataService)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _dataService = dataService;
            _restClient.UseNewtonsoftJson();
        }
        // GET: TruckBookingController
        public async Task<IActionResult> Index()
        {
            var requestUrl = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "1"
                                ? "TruckBookings/GetList" : User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2"
                                ? $"TruckBookings/GetByCompany?id={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}"
                                : $"TruckBookings/GetByCustomer?id={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            var truckBookings = await _restClient.GetAsync<IEnumerable<TruckBookingDTO>>(request);
            if (truckBookings == null)
            {
                truckBookings = new List<TruckBookingDTO>();
            }

            return View(truckBookings);
        }
        
        public async Task<IActionResult> ActiveBookings()
        {
            var requestUrl = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "1"
                                ? "TruckBookings/GetActiveList" : User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2"
                                ? $"TruckBookings/GetActiveByCompany?id={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}"
                                : $"TruckBookings/GetActiveByCustomer?id={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            var truckBookings = await _restClient.GetAsync<IEnumerable<TruckBookingDTO>>(request);
            if (truckBookings == null)
            {
                truckBookings = new List<TruckBookingDTO>();
            }

            return View(truckBookings);
        }

        // GET: TruckBookingController/Details/
        public async Task<IActionResult> Details(int truckBookingId)
        {

            var request = new RestRequest($"TruckBookings/GetSingle?id={truckBookingId}", DataFormat.Json);
            var truckBooking = await _restClient.GetAsync<TruckBookingLocationViewModel>(request);
            
            if (truckBooking == null)
            {
                truckBooking = new TruckBookingLocationViewModel();
            }
            else
            {
                truckBooking.TruckLocationResponse = await _dataService.GetCurrentTruckLocation(truckBooking.Truck.TruckImeiNumber);
            }
            

            return View(truckBooking);
        }

        // GET: TruckBookingController/Create
        public async Task<IActionResult> Create()
        {
            return View(new TruckBookingRequestViewModel 
            {
                ServiceProviders = await GetServiceProviders()
            });
        }

        // POST: TruckBookingController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] TruckBookingRequest truckBookingRequest, IFormCollection bookingForm)
        {
            truckBookingRequest.CustomerId = Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value);
            
            var request = new RestRequest("TruckBookings/Create")
                    .AddJsonBody(truckBookingRequest);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            string jsonString = JsonConvert.SerializeObject(response.Data.Data);
            var responseObject = JsonConvert.DeserializeObject<ReturnObject>(jsonString);
            var newBooking = responseObject.Entity;

            if (response.StatusCode == HttpStatusCode.Created)
            {
                //Add service providers
                var serviceProviders = new List<CustomerServiceProviderDTO>();
                serviceProviders.Add(new CustomerServiceProviderDTO
                {
                    ServiceProviderId = Convert.ToInt32(bookingForm["INS"]),
                    CustomerId = Convert.ToInt32(truckBookingRequest.CustomerId),
                    BookingId = newBooking.TruckBookingId
                });

                serviceProviders.Add(new CustomerServiceProviderDTO
                {
                    ServiceProviderId = Convert.ToInt32(bookingForm["FUM"]),
                    CustomerId = Convert.ToInt32(truckBookingRequest.CustomerId),
                    BookingId = newBooking.TruckBookingId
                });

                serviceProviders.Add(new CustomerServiceProviderDTO
                {
                    ServiceProviderId = Convert.ToInt32(bookingForm["CLR"]),
                    CustomerId = Convert.ToInt32(truckBookingRequest.CustomerId),
                    BookingId = newBooking.TruckBookingId
                });

                request = new RestRequest("CustomerServiceProviders/Create")
                    .AddJsonBody(serviceProviders);
                response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(truckBookingRequest);
        }

        // GET: TruckBookingController/Edit/
        public async Task<IActionResult> Edit(int truckBookingId)
        {
            return View(new TruckBookingViewModel
            {
                TruckBooking = await GetTruckBooking(truckBookingId),
                Customers = await _dataService.GetCustomers(),
                Trucks = await _dataService.GetTrucks(),
                EventStages = await _dataService.GetEventStages()
            });
        }

        // POST: TruckBookingController/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int truckBookingId, [Bind] TruckBookingViewModel truckBookingViewModel, int eventStageId)
        {
            var truckBookingDTO = truckBookingViewModel.TruckBooking;
            var request = new RestRequest($"TruckBookings/Update?id={truckBookingId}&eventStageId={eventStageId}")
                    .AddJsonBody(truckBookingDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.PUT);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckBookingViewModel
            {
                TruckBooking = await GetTruckBooking(truckBookingId),
                Customers = await _dataService.GetCustomers(),
                Trucks = await _dataService.GetTrucks()
            });
        }
        public class ReturnObject
        {
            public TruckBookingDTO Entity { get; set; }
            public int State { get; set; }
        }
        public async Task<TruckBookingDTO> GetTruckBooking(int truckBookingId)
        {
            var request = new RestRequest($"TruckBookings/GetSingle?id={truckBookingId}", DataFormat.Json);
            var truckBooking = await _restClient.GetAsync<TruckBookingDTO>(request);
            if (truckBooking == null)
            {
                truckBooking = new TruckBookingDTO();
            }

            return truckBooking;
        }

        public async Task<IEnumerable<ServiceProviderDTO>> GetServiceProviders()
        {
            var requestUrl = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2" ?
                            $"ServiceProviders/GetServiceProvidersByCompany?companyId={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}"
                            : "ServiceProviders/GetList";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            var ServiceProviders = await _restClient.GetAsync<IEnumerable<ServiceProviderDTO>>(request);
            if (ServiceProviders == null)
            {
                ServiceProviders = new List<ServiceProviderDTO>();
            }

            return ServiceProviders;
        }
    }
}
