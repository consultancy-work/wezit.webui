﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.DataModels.ViewModels;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Helpers.General;
using Wezit.WebUI.SimpleService;

namespace Wezit.WebUI.Controllers
{
    [Authorize]
    public class TruckCompanySubscriptionController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;
        private readonly IDataService _dataService;

        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }

        public TruckCompanySubscriptionController(IConfiguration configuration, 
            IRestClient restClient,
            IDataService dataService)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _dataService = dataService;
            _restClient.UseNewtonsoftJson();
        }
        // GET: TruckCompanySubscriptionController
        public async Task<IActionResult> Index()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var requestURl = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2")
                                ? $"TruckCompanySubscriptions/GetByCompany?companyId={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}" : "TruckCompanySubscriptions/GetList";
            
            var request = new RestRequest(requestURl, DataFormat.Json);
            var truckCompanySubscriptions = await _restClient.GetAsync<IEnumerable<TruckCompanySubscriptionDTO>>(request);
            if(truckCompanySubscriptions == null)
            {
                truckCompanySubscriptions = new List<TruckCompanySubscriptionDTO>();
            }

            return View(truckCompanySubscriptions);
        }

        // GET: TruckCompanySubscriptionController/Details/
        public async Task<IActionResult> Details(int truckCompanySubscriptionId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TruckCompanySubscriptions/GetSingle?id={truckCompanySubscriptionId}", DataFormat.Json);
            var truckCompanySubscription = await _restClient.GetAsync<TruckCompanySubscriptionDTO>(request);
            if (truckCompanySubscription == null)
            {
                truckCompanySubscription = new TruckCompanySubscriptionDTO();
            }

            return View(truckCompanySubscription);
        }

        // GET: TruckCompanySubscriptionController/Create
        public async Task<IActionResult> Create()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var truckOwners = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2")
                ? await _dataService.GetTruckOwners() : Enumerable.Empty<TruckOwnerDTO>();
            return View(new TruckCompanySubscriptionViewModel {
                BandSubscriptions = await _dataService.GetBandSubscriptions(),
                TruckOwners = truckOwners,
            });
        }

        // POST: TruckCompanySubscriptionController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] TruckCompanySubscriptionViewModel truckCompanySubscriptionViewModel)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var truckCompanySubscriptionDTO = truckCompanySubscriptionViewModel.TruckCompanySubscription;
            var request = new RestRequest("TruckCompanySubscriptions/Create")
                    .AddJsonBody(truckCompanySubscriptionDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckCompanySubscriptionViewModel
            {
                BandSubscriptions = await _dataService.GetBandSubscriptions(),
                TruckOwners = await _dataService.GetTruckOwners()
            });
        }

        // GET: TruckCompanySubscriptionController/Edit/
        public async Task<IActionResult> Edit(int truckCompanySubscriptionId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            return View(new TruckCompanySubscriptionViewModel
            {
                TruckCompanySubscription = await GetTruckCompanySubscription(truckCompanySubscriptionId),
                BandSubscriptions = await _dataService.GetBandSubscriptions(),
                TruckOwners = await _dataService.GetTruckOwners()
            });
        }

        // POST: TruckCompanySubscriptionController/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int truckCompanySubscriptionId, [Bind] TruckCompanySubscriptionViewModel truckCompanySubscriptionViewModel)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var truckCompanySubscriptionDTO = truckCompanySubscriptionViewModel.TruckCompanySubscription;
            var request = new RestRequest($"TruckCompanySubscriptions/Update?id={truckCompanySubscriptionId}")
                    .AddJsonBody(truckCompanySubscriptionDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.PUT);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckCompanySubscriptionViewModel
            {
                TruckCompanySubscription = await GetTruckCompanySubscription(truckCompanySubscriptionId),
                BandSubscriptions = await _dataService.GetBandSubscriptions(),
                TruckOwners = await _dataService.GetTruckOwners()
            });
        }

        // GET: TruckCompanySubscriptionController/Delete/
        public async Task<IActionResult> Delete(int truckCompanySubscriptionId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TruckCompanySubscriptions/GetSingle?id={truckCompanySubscriptionId}", DataFormat.Json);
            var truckCompanySubscription = await _restClient.GetAsync<TruckCompanySubscriptionDTO>(request);
            if (truckCompanySubscription == null)
            {
                truckCompanySubscription = new TruckCompanySubscriptionDTO();
            }

            return View(truckCompanySubscription);
        }

        // POST: TruckCompanySubscriptionController/Delete/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int truckCompanySubscriptionId, IFormCollection collection)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TruckCompanySubscriptions/Delete?id={truckCompanySubscriptionId}");
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("The item was deleted successfully 🚮", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckCompanySubscriptionDTO());
        }

        public async Task<TruckCompanySubscriptionDTO> GetTruckCompanySubscription(int truckCompanySubscriptionId)
        {
            var request = new RestRequest($"TruckCompanySubscriptions/GetSingle?id={truckCompanySubscriptionId}", DataFormat.Json);
            var truckCompanySubscription = await _restClient.GetAsync<TruckCompanySubscriptionDTO>(request);
            if (truckCompanySubscription == null)
            {
                truckCompanySubscription = new TruckCompanySubscriptionDTO();
            }

            return truckCompanySubscription;
        }
    }
}
