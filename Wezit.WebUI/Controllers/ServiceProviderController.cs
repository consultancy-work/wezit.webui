﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.DataModels.ViewModels;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Helpers.General;
using Wezit.WebUI.SimpleService;

namespace Wezit.WebUI.Controllers
{
    [Authorize]
    public class ServiceProviderController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;
        private readonly IDataService _dataService;

        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }

        public ServiceProviderController(IConfiguration configuration, 
            IRestClient restClient,
            IDataService dataService)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _dataService = dataService;
            _restClient.UseNewtonsoftJson();
        }
        // GET: ServiceProviderController
        public async Task<IActionResult> Index()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var requestUrl = "ServiceProviders/GetList";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            var ServiceProviders = await _restClient.GetAsync<IEnumerable<ServiceProviderDTO>>(request);
            if(ServiceProviders == null)
            {
                ServiceProviders = new List<ServiceProviderDTO>();
            }

            return View(ServiceProviders);
        }

        // GET: ServiceProviderController/Details/
        public async Task<IActionResult> Details(int ServiceProviderId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"ServiceProviders/GetSingle?id={ServiceProviderId}", DataFormat.Json);
            var ServiceProvider = await _restClient.GetAsync<ServiceProviderDTO>(request);
            if (ServiceProvider == null)
            {
                ServiceProvider = new ServiceProviderDTO();
            }

            return View(ServiceProvider);
        }

        // GET: ServiceProviderController/Create
        public async Task<IActionResult> Create()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }

            return View(new ServiceProviderViewModel
            {
                ServiceProviderTypes = await GetServiceProviderTypes()
            });
        }

        // POST: ServiceProviderController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] ServiceProviderDTO serviceProvider)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }

            var request = new RestRequest("ServiceProviders/Create")
                    .AddJsonBody(serviceProvider);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new ServiceProviderViewModel
            {
                ServiceProviderTypes = await GetServiceProviderTypes()
            });
        }

        // GET: ServiceProviderController/Edit/
        public async Task<IActionResult> Edit(int ServiceProviderId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            return View(await GetServiceProvider(ServiceProviderId));
        }

        // POST: ServiceProviderController/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int ServiceProviderId, [Bind] ServiceProviderDTO serviceProvider)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"ServiceProviders/Update?id={ServiceProviderId}")
                    .AddJsonBody(serviceProvider);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.PUT);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(await GetServiceProvider(ServiceProviderId));
        }

        // GET: ServiceProviderController/Delete/
        public async Task<IActionResult> Delete(int ServiceProviderId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"ServiceProviders/GetSingle?id={ServiceProviderId}", DataFormat.Json);
            var ServiceProvider = await _restClient.GetAsync<ServiceProviderDTO>(request);
            if (ServiceProvider == null)
            {
                ServiceProvider = new ServiceProviderDTO();
            }

            return View(ServiceProvider);
        }

        // POST: ServiceProviderController/Delete/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int ServiceProviderId, IFormCollection collection)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"ServiceProviders/Delete?id={ServiceProviderId}");
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("The item was deleted successfully 🚮", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new ServiceProviderDTO());
        }

        public async Task<ServiceProviderDTO> GetServiceProvider(int ServiceProviderId)
        {
            var request = new RestRequest($"ServiceProviders/GetSingle?id={ServiceProviderId}", DataFormat.Json);
            var ServiceProvider = await _restClient.GetAsync<ServiceProviderDTO>(request);
            if (ServiceProvider == null)
            {
                ServiceProvider = new ServiceProviderDTO();
            }

            return ServiceProvider;
        }
        public async Task<JsonResult> GetServiceProviders()
        {
            var requestUrl = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2" ?
                            $"ServiceProviders/GetServiceProvidersByCompany?companyId={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}"
                            : "ServiceProviders/GetList";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            var ServiceProviders = await _restClient.GetAsync<IEnumerable<ServiceProviderDTO>>(request);
            if (ServiceProviders == null)
            {
                ServiceProviders = new List<ServiceProviderDTO>();
            }

            return Json(ServiceProviders);
        }
        public async Task<IEnumerable<ServiceProviderTypeDTO>> GetServiceProviderTypes()
        {
            var request = new RestRequest("ServiceProviderTypes/GetList", DataFormat.Json);
            var ServiceProviders = await _restClient.GetAsync<IEnumerable<ServiceProviderTypeDTO>>(request);
            if (ServiceProviders == null)
            {
                ServiceProviders = new List<ServiceProviderTypeDTO>();
            }

            return ServiceProviders;
        }
        public async Task<JsonResult> GetLoadTypes()
        {
            var request = new RestRequest($"ServiceProviders/GetCategoryList", DataFormat.Json);
            var ServiceProviders = await _restClient.GetAsync<IEnumerable<DeliveryCategoryDTO>>(request);
            if (ServiceProviders == null)
            {
                ServiceProviders = new List<DeliveryCategoryDTO>();
            }

            return Json(ServiceProviders);
        }
    }
}
