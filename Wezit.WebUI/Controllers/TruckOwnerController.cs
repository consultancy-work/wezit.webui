﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.WebUI.Data;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Helpers.General;
using Wezit.WebUI.Models;

namespace Wezit.WebUI.Controllers
{
    [Authorize]
    public class TruckOwnerController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;


        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }

        public TruckOwnerController(IConfiguration configuration,
            IRestClient restClient, UserManager<ApplicationUser> userManager,
                                      SignInManager<ApplicationUser> signInManager)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _restClient.UseNewtonsoftJson();
            _userManager = userManager;
            _signInManager = signInManager;
        }
        // GET: TruckOwnerController
        public async Task<IActionResult> Index()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest("TruckOwners/GetList", DataFormat.Json);
            var truckOwners = await _restClient.GetAsync<IEnumerable<TruckOwnerDTO>>(request);
            if (truckOwners == null)
            {
                truckOwners = new List<TruckOwnerDTO>();
            }

            return View(truckOwners);
        }

        // GET: TruckOwnerController/Details
        public async Task<IActionResult> Details(int truckOwnerId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TruckOwners/GetSingle?id={truckOwnerId}", DataFormat.Json);
            var truckOwner = await _restClient.GetAsync<TruckOwnerDTO>(request);
            if (truckOwner == null)
            {
                truckOwner = new TruckOwnerDTO();
            }

            return View(truckOwner);
        }

        // GET: TruckOwnerController/Create
        [AllowAnonymous]
        public async Task<ActionResult> Create()
        {
            return View(new TruckOwnerViewModel
            {
                ServiceProviders = await GetServiceProviders()
            });
        }

        // POST: TruckOwnerController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Create([Bind] TruckOwnerViewModel truckOwnerViewModel, IFormCollection truckCompanyForm)
        {
            TruckOwnerDTO truckOwner = truckOwnerViewModel;
            var request = new RestRequest("TruckOwners/Create")
                    .AddJsonBody(truckOwner);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            string jsonString = JsonConvert.SerializeObject(response.Data.Data);
            var responseObject = JsonConvert.DeserializeObject<ReturnObject>(jsonString);
            var newtruckOwner = responseObject.Entity;

            if (response.StatusCode == HttpStatusCode.Created)
            {
                //Add service providers
                var serviceProviders = new List<TruckOwnerServiceProviderDTO>();
                serviceProviders.Add(new TruckOwnerServiceProviderDTO
                {
                    ServiceProviderId = Convert.ToInt32(truckCompanyForm["INS"]),
                    TruckOwnerId = newtruckOwner.TruckCompanyId
                });
                
                serviceProviders.Add(new TruckOwnerServiceProviderDTO
                {
                    ServiceProviderId = Convert.ToInt32(truckCompanyForm["MTC"]),
                    TruckOwnerId = newtruckOwner.TruckCompanyId
                });
                
                serviceProviders.Add(new TruckOwnerServiceProviderDTO
                {
                    ServiceProviderId = Convert.ToInt32(truckCompanyForm["FC"]),
                    TruckOwnerId = newtruckOwner.TruckCompanyId
                });

                request = new RestRequest("TruckOwnerServiceProviders/Create")
                    .AddJsonBody(serviceProviders);
                response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

                //AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                var user = new ApplicationUser
                {
                    UserName = truckOwnerViewModel.EmailAddress,
                    Email = truckOwnerViewModel.EmailAddress,
                    FirstName = truckOwnerViewModel.ContactPersonFirstName,
                    LastName = truckOwnerViewModel.ContactPersonLastName,
                    GroupId = 2,
                    UserId = newtruckOwner.TruckCompanyId
                };

                var result = await _userManager.CreateAsync(user, truckOwnerViewModel.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);

                    return RedirectToAction("Login", "Account");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckOwnerDTO());
        }

        // GET: TruckOwnerController/Edit
        public async Task<IActionResult> Edit(int truckOwnerId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TruckOwners/GetSingle?id={truckOwnerId}", DataFormat.Json);
            var truckOwner = await _restClient.GetAsync<TruckOwnerDTO>(request);
            if (truckOwner == null)
            {
                truckOwner = new TruckOwnerDTO();
            }

            return View(truckOwner);
        }

        // POST: TruckOwnerController/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int truckOwnerId, [Bind] TruckOwnerDTO truckOwnerDTO)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TruckOwners/Update?id={truckOwnerId}")
                    .AddJsonBody(truckOwnerDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.PUT);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckOwnerDTO());
        }

        // GET: TruckOwnerController/Delete
        public async Task<IActionResult> Delete(int truckOwnerId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TruckOwners/GetSingle?id={truckOwnerId}", DataFormat.Json);
            var truckOwner = await _restClient.GetAsync<TruckOwnerDTO>(request);
            if (truckOwner == null)
            {
                truckOwner = new TruckOwnerDTO();
            }

            return View(truckOwner);
        }

        // POST: TruckOwnerController/Delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int truckOwnerId, IFormCollection collection)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TruckOwners/Delete?id={truckOwnerId}");
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("The item was deleted successfully 🚮", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckOwnerDTO());
        }
        public class ReturnObject
        {
            public TruckOwnerDTO Entity { get; set; }
            public int State { get; set; }
        }

        private async Task<IEnumerable<ServiceProviderDTO>> GetServiceProviders()
        {
            var requestUrl = "ServiceProviders/GetList";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            var ServiceProviders = await _restClient.GetAsync<IEnumerable<ServiceProviderDTO>>(request);
            if (ServiceProviders == null)
            {
                ServiceProviders = new List<ServiceProviderDTO>();
            }
            return ServiceProviders;
        }
    }
}
