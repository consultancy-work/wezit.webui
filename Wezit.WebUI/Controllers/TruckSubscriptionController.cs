﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Helpers.General;
using Wezit.WebUI.SimpleService;

namespace Wezit.WebUI.Controllers
{
    [Authorize]
    public class TrucksSubscribedController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;
        private readonly IDataService _dataService;

        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }

        public TrucksSubscribedController(IConfiguration configuration, 
            IRestClient restClient,
            IDataService dataService)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _dataService = dataService;
            _restClient.UseNewtonsoftJson();
        }
        // GET: TrucksSubscribedController
        public async Task<IActionResult> Index()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var requestURl = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2")
                                ? $"TrucksSubscribed/GetByCompany?companyId={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}" : "TrucksSubscribed/GetList";
            
            var request = new RestRequest(requestURl, DataFormat.Json);
            var trucksSubscribeds = await _restClient.GetAsync<IEnumerable<TrucksSubscribedDTO>>(request);
            if(trucksSubscribeds == null)
            {
                trucksSubscribeds = new List<TrucksSubscribedDTO>();
            }

            return View(trucksSubscribeds);
        }

        // GET: TrucksSubscribedController/Details/
        public async Task<IActionResult> Details(int subscriptionId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TrucksSubscribed/GetSingle?id={subscriptionId}", DataFormat.Json);
            var trucksSubscribed = await _restClient.GetAsync<TrucksSubscribedDTO>(request);
            if (trucksSubscribed == null)
            {
                trucksSubscribed = new TrucksSubscribedDTO();
            }

            return View(trucksSubscribed);
        }

        // GET: TrucksSubscribedController/Create
        public async Task<IActionResult> Create()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var requestURl = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2")
                               ? $"TruckCompanySubscriptions/GetByCompany?companyId={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}" : "TruckCompanySubscriptions/GetList";

            var request = new RestRequest(requestURl, DataFormat.Json);
            var truckCompanySubscriptions = await _restClient.GetAsync<IEnumerable<TruckCompanySubscriptionDTO>>(request);
            return View(new TrucksSubscribedDTO 
            {
                TruckSubscriptionId = truckCompanySubscriptions.FirstOrDefault().TruckSubscriptionId
            });
        }

        // POST: TrucksSubscribedController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] TrucksSubscribedDTO trucksSubscribed)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest("TrucksSubscribed/Create")
                    .AddJsonBody(trucksSubscribed);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TrucksSubscribedDTO());
        }

        // GET: TrucksSubscribedController/Edit/
        public async Task<IActionResult> Edit(int subscriptionId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            return View(await GetTrucksSubscribed(subscriptionId));
        }

        // POST: TrucksSubscribedController/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int subscriptionId, [Bind] TrucksSubscribedDTO trucksSubscribed)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TrucksSubscribed/Update?id={subscriptionId}")
                    .AddJsonBody(trucksSubscribed);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.PUT);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(await GetTrucksSubscribed(subscriptionId));
        }

        // GET: TrucksSubscribedController/Delete/
        public async Task<IActionResult> Delete(int subscriptionId)
        {
            var request = new RestRequest($"TrucksSubscribed/GetSingle?id={subscriptionId}", DataFormat.Json);
            var trucksSubscribed = await _restClient.GetAsync<TrucksSubscribedDTO>(request);
            if (trucksSubscribed == null)
            {
                trucksSubscribed = new TrucksSubscribedDTO();
            }

            return View(trucksSubscribed);
        }

        // POST: TrucksSubscribedController/Delete/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int subscriptionId, IFormCollection collection)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"TrucksSubscribed/Delete?id={subscriptionId}");
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("The item was deleted successfully 🚮", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(await GetTrucksSubscribed(subscriptionId));
        }

        public async Task<TrucksSubscribedDTO> GetTrucksSubscribed(int subscriptionId)
        {
            var request = new RestRequest($"TrucksSubscribed/GetSingle?id={subscriptionId}", DataFormat.Json);
            var trucksSubscribed = await _restClient.GetAsync<TrucksSubscribedDTO>(request);
            if (trucksSubscribed == null)
            {
                trucksSubscribed = new TrucksSubscribedDTO();
            }

            return trucksSubscribed;
        }

        public async Task<JsonResult> GetAvailableTrucksSubscribed()
        {
            var request = new RestRequest($"TrucksSubscribed/AvailableTrucksSubscribed?companyId={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))}", DataFormat.Json);
            var trucksSubscribed = await _restClient.GetAsync<TrucksSubscribedDTO>(request);
            if (trucksSubscribed == null)
            {
                trucksSubscribed = new TrucksSubscribedDTO();
            }

            return Json(trucksSubscribed);
        }
    }
}
