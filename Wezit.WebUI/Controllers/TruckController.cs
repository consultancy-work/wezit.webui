﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.DataModels.ViewModels;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Helpers.General;
using Wezit.WebUI.Models;
using Wezit.WebUI.SimpleService;

namespace Wezit.WebUI.Controllers
{
    [Authorize]
    public class TruckController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;
        private readonly IDataService _dataService;

        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }

        public string GoogleApiKey
        {
            get
            {
                return _configuration["GoogleApiKey"];
            }
        }
        public TruckController(IConfiguration configuration, 
            IRestClient restClient,
            IDataService dataService)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _dataService = dataService;
            _restClient.UseNewtonsoftJson();
        }
        // GET: TruckController
        public async Task<IActionResult> Index()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var requestUrl = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2" ?
                            $"Trucks/GetTrucksByCompany?companyId={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}"
                            : "Trucks/GetList";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            var trucks = await _restClient.GetAsync<IEnumerable<TruckDTO>>(request);
            if(trucks == null)
            {
                trucks = new List<TruckDTO>();
            }

            return View(trucks);
        }

        // GET: TruckController/Details/
        public async Task<IActionResult> Details(int truckId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"Trucks/GetSingle?id={truckId}", DataFormat.Json);
            var truck = await _restClient.GetAsync<TruckLocationViewModel>(request);
            if (truck == null)
            {
                truck = new TruckLocationViewModel();
            }
            else
            {
                _restClient.BaseUrl = new System.Uri("http://maps.googleapis.com/maps/api/");
                truck.TruckLocationResponse = await _dataService.GetCurrentTruckLocation(truck.TruckImeiNumber);
            }

            return View(truck);
        }

        // GET: TruckController/Create
        public async Task<IActionResult> Create()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var truckOwners = (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2")
                ? await _dataService.GetTruckOwners() : Enumerable.Empty<TruckOwnerDTO>();
            return View(new TruckViewModel { 
                TruckOwners = truckOwners
            });
        }

        // POST: TruckController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] TruckViewModel truckViewModel)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var truckDTO = truckViewModel.Truck;
            var request = new RestRequest("Trucks/Create")
                    .AddJsonBody(truckDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckViewModel
            {
                TruckOwners = await _dataService.GetTruckOwners()
            });
        }

        // GET: TruckController/Edit/
        public async Task<IActionResult> Edit(int truckId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            return View(new TruckViewModel
            {
                Truck = await GetTruck(truckId),
                TruckOwners = await _dataService.GetTruckOwners()
            });
        }

        // POST: TruckController/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int truckId, [Bind] TruckViewModel truckViewModel)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var truckDTO = truckViewModel.Truck;
            var request = new RestRequest($"Trucks/Update?id={truckId}")
                    .AddJsonBody(truckDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.PUT);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckViewModel
            {
                Truck = await GetTruck(truckId),
                TruckOwners = await _dataService.GetTruckOwners()
            });
        }

        // GET: TruckController/Delete/
        public async Task<IActionResult> Delete(int truckId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"Trucks/GetSingle?id={truckId}", DataFormat.Json);
            var truck = await _restClient.GetAsync<TruckDTO>(request);
            if (truck == null)
            {
                truck = new TruckDTO();
            }

            return View(truck);
        }

        // POST: TruckController/Delete/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int truckId, IFormCollection collection)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"Trucks/Delete?id={truckId}");
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("The item was deleted successfully 🚮", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new TruckDTO());
        }

        public async Task<TruckDTO> GetTruck(int truckId)
        {
            var request = new RestRequest($"Trucks/GetSingle?id={truckId}", DataFormat.Json);
            var truck = await _restClient.GetAsync<TruckDTO>(request);
            if (truck == null)
            {
                truck = new TruckDTO();
            }
            

            return truck;
        }
        public async Task<JsonResult> GetTrucks()
        {
            var requestUrl = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "2" ?
                            $"Trucks/GetTrucksByCompany?companyId={User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.PrimarySid))?.Value}"
                            : "Trucks/GetList";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            var trucks = await _restClient.GetAsync<IEnumerable<TruckDTO>>(request);
            if (trucks == null)
            {
                trucks = new List<TruckDTO>();
            }

            return Json(trucks);
        }
        public async Task<JsonResult> GetTruckTypes()
        {
            var request = new RestRequest("Trucks/GetTruckTypeList", DataFormat.Json);
            var trucks = await _restClient.GetAsync<IEnumerable<TruckTypeDTO>>(request);
            if (trucks == null)
            {
                trucks = new List<TruckTypeDTO>();
            }

            return Json(trucks);
        }
        public async Task<JsonResult> GetLoadTypes()
        {
            var request = new RestRequest($"Trucks/GetCategoryList", DataFormat.Json);
            var trucks = await _restClient.GetAsync<IEnumerable<DeliveryCategoryDTO>>(request);
            if (trucks == null)
            {
                trucks = new List<DeliveryCategoryDTO>();
            }

            return Json(trucks);
        }
    }
}
