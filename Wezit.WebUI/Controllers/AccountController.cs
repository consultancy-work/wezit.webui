﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.WebUI.Data;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Models;

namespace Wezit.WebUI.Controllers
{

    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IRestClient _restClient;
        private readonly IConfiguration _configuration;
        private readonly INotificationHelper _notificationHelper;
        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }
        public AccountController(UserManager<ApplicationUser> userManager,
                                      SignInManager<ApplicationUser> signInManager,
                                      IRestClient restClient,
                                      IConfiguration configuration,
                                      INotificationHelper notificationHelper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _restClient = restClient;
            _configuration = configuration;
            _restClient.BaseUrl = new Uri(BaseUrl);
            _notificationHelper = notificationHelper;
        }
       
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Register()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    GroupId = model.GroupId
                };

                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);

                    return RedirectToAction("Index", "TruckBooking");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");

            }
            return View(model);
        }


        [AllowAnonymous]
        public async Task<IActionResult> RegisterSysAdmin()
        {
            var user = new ApplicationUser
            {
                UserName = "v2vinkhumb0@gmail.com",
                Email = "v2vinkhumb0@gmail.com",
                FirstName = "SYS",
                LastName = "ADMIN",
                GroupId = 1,
                UserId = 0
            };

            var result = await _userManager.CreateAsync(user, "Password8#");

            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, isPersistent: false);
                return RedirectToAction("Index", "TruckBooking");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            ModelState.AddModelError(string.Empty, "Invalid Login Attempt");


            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel user)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(user.Email, user.Password, user.RememberMe, false);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "TruckBooking");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");

            }
            return View(user);
        }
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Login");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> VerifyEmail([FromQuery] string ver)
        {
            //var customerId = AesOperation.DecryptString(_configuration["EncKey"], ver);
            var request = new RestRequest($"VerificationCodes/VerifyEmail?customerId={ver}", DataFormat.Json);
            var response = await _restClient.GetAsync<FeedbackMessage>(request);
            if (!response.HasErrorOccured)
            {
                request = new RestRequest($"Customers/GetSingle?id={ver}", DataFormat.Json);
                var customer = await _restClient.GetAsync<CustomerDTO>(request);
                if (customer is not null)
                {
                    var endPoint = Url.Action("Login", "Account");
                    var url = $"http://{Request.Host}{endPoint}";
                    var subject = "Wezit Account Verification";
                    var body = "Thanks for signing up! Your account has been verified, you can login with your credentials.<br />Please click this link to activate your account: " + url + " \r\n";

                    _notificationHelper.SendEmail(customer.EmailAddress, subject, body);
                }
                
            }
            return RedirectToAction(nameof(Login));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult VerifyPhone([FromQuery] string pn)
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> VerifyCode([FromBody] string pn, int code)
        {
            var request = new RestRequest($"VerificationCodes/Verify?phoneNumber={pn}&code={code}", DataFormat.Json);
            var response = await _restClient.GetAsync<FeedbackMessage>(request);
            return Json(response);
        }
    }
}
