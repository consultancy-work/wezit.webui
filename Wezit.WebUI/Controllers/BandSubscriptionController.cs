﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.WebUI.Helpers;
using Wezit.WebUI.Helpers.General;

namespace Wezit.WebUI.Controllers
{
    [Authorize]
    public class BandSubscriptionController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;

        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }

        public BandSubscriptionController(IConfiguration configuration, 
            IRestClient restClient)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _restClient.UseNewtonsoftJson();
        }
        // GET: BandSubscriptionController
        public async Task<IActionResult> Index()
        {
            if(User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value == "3")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest("BandSubscriptions/GetList", DataFormat.Json);
            var bandSubscriptions = await _restClient.GetAsync<IEnumerable<BandSubscriptionDTO>>(request);
            if(bandSubscriptions == null)
            {
                bandSubscriptions = new List<BandSubscriptionDTO>();
            }

            return View(bandSubscriptions);
        }

        // GET: BandSubscriptionController/Details
        public async Task<IActionResult> Details(int bandSubscriptionId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"BandSubscriptions/GetSingle?id={bandSubscriptionId}", DataFormat.Json);
            var bandSubscription = await _restClient.GetAsync<BandSubscriptionDTO>(request);
            if (bandSubscription == null)
            {
                bandSubscription = new BandSubscriptionDTO();
            }

            return View(bandSubscription);
        }

        // GET: BandSubscriptionController/Create
        public ActionResult Create()
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            return View(new BandSubscriptionDTO());
        }

        // POST: BandSubscriptionController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] BandSubscriptionDTO bandSubscriptionDTO)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest("BandSubscriptions/Create")
                    .AddJsonBody(bandSubscriptionDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new BandSubscriptionDTO());
        }

        // GET: BandSubscriptionController/Edit
        public async Task<IActionResult> Edit(int bandSubscriptionId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"BandSubscriptions/GetSingle?id={bandSubscriptionId}", DataFormat.Json);
            var bandSubscription = await _restClient.GetAsync<BandSubscriptionDTO>(request);
            if (bandSubscription == null)
            {
                bandSubscription = new BandSubscriptionDTO();
            }

            return View(bandSubscription);
        }

        // POST: BandSubscriptionController/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int bandSubscriptionId, [Bind] BandSubscriptionDTO bandSubscriptionDTO)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"BandSubscriptions/Update?id={bandSubscriptionId}")
                    .AddJsonBody(bandSubscriptionDTO);
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.PUT);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("Process complete, Wezit updated", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new BandSubscriptionDTO());
        }

        // GET: BandSubscriptionController/Delete
        public async Task<IActionResult> Delete(int bandSubscriptionId)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"BandSubscriptions/GetSingle?id={bandSubscriptionId}", DataFormat.Json);
            var bandSubscription = await _restClient.GetAsync<BandSubscriptionDTO>(request);
            if (bandSubscription == null)
            {
                bandSubscription = new BandSubscriptionDTO();
            }

            return View(bandSubscription);
        }

        // POST: BandSubscriptionController/Delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int bandSubscriptionId, IFormCollection collection)
        {
            if (User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.GroupSid))?.Value != "1")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            var request = new RestRequest($"BandSubscriptions/Delete?id={bandSubscriptionId}");
            var response = await _restClient.ExecuteAsync<FeedbackMessage>(request, Method.POST);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                AppContextHelper.SetToastMessage("The item was deleted successfully 🚮", MessageType.Success, 1, Response);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var responseMessage = response.Data.Message ?? response.ErrorMessage;
                AppContextHelper.SetToastMessage(responseMessage, MessageType.Danger, 1, Response);
                ModelState.AddModelError("", responseMessage);
            }

            return View(new BandSubscriptionDTO());
        }
    }
}
