﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wezit.WebUI.Models
{
    public class TokenResponse
    {
        public int Code { get; set; }
        public long ExpiresIn { get; set; }
        public string AccessToken { get; set; }
        public string Result { get; set; }
    }
}
