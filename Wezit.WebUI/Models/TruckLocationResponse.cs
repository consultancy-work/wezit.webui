﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wezit.WebUI.Models
{
    public class TruckLocationResponse
    {
        public int Code { get; set; }
        public string Lng { get; set; }
        public string Lat { get; set; }
        public string Address { get; set; }
    }
}
