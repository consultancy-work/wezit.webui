﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Wezit.WebUI.Data;

namespace Wezit.WebUI.Models
{
    public class WezitIdentityDBContext : IdentityDbContext
    {

        private readonly DbContextOptions _options;

        public WezitIdentityDBContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>()
               .Property(e => e.FirstName)
               .HasMaxLength(250);

            modelBuilder.Entity<ApplicationUser>()
                .Property(e => e.LastName)
                .HasMaxLength(250);

            modelBuilder.Entity<ApplicationUser>()
                .Property(e => e.GroupId);

            modelBuilder.Entity<ApplicationUser>()
                .Property(e => e.UserId);

        }
    }
}
