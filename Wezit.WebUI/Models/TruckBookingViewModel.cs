﻿using System.Collections.Generic;
using Wezit.DataModels.DTO;

namespace Wezit.DataModels.ViewModels
{
    public class TruckBookingViewModel
    {
        public TruckBookingDTO TruckBooking { get; set; }
        public IEnumerable<CustomerDTO> Customers { get; set; }
        public IEnumerable<TruckDTO> Trucks { get; set; }
        public IEnumerable<EventLogStageDTO> EventStages { get; set; }
    }
}
