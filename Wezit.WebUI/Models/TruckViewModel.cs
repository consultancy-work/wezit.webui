﻿using System.Collections.Generic;
using Wezit.DataModels.DTO;

namespace Wezit.DataModels.ViewModels
{
    public class TruckViewModel
    {
        public TruckDTO Truck { get; set; }
        public IEnumerable<TruckOwnerDTO> TruckOwners { get; set; }
    }
}
