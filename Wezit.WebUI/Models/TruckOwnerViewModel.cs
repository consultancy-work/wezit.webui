﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Wezit.DataModels.DTO;

namespace Wezit.WebUI.Models
{
    public class TruckOwnerViewModel : TruckOwnerDTO
    {

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Password and confirmation password not match.")]
        public string ConfirmPassword { get; set; }
        public IEnumerable<ServiceProviderDTO> ServiceProviders { get; set; } 
    }
}
