﻿using System.Collections.Generic;
using Wezit.DataModels.DTO;

namespace Wezit.DataModels.ViewModels
{
    public class TruckCompanySubscriptionViewModel
    {
        public TruckCompanySubscriptionDTO TruckCompanySubscription { get; set; }
        public IEnumerable<BandSubscriptionDTO> BandSubscriptions { get; set; }
        public IEnumerable<TruckOwnerDTO> TruckOwners { get; set; }
    }
}
