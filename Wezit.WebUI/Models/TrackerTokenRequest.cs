﻿using System.Text.Json.Serialization;

namespace Wezit.WebUI.Models
{
    public class TrackerTokenRequest
    {
        [JsonPropertyName("appid")]
        public string appid { get; set; }
        [JsonPropertyName("time")]
        public  long time { get; set; }
        [JsonPropertyName("signature")]
        public string signature { get; set; }
    }
}
