﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;

namespace Wezit.WebUI.Models
{
    public class TruckLocationViewModel:TruckDTO
    {
        public TruckLocationResponse TruckLocationResponse { get; set; }
    }
}
