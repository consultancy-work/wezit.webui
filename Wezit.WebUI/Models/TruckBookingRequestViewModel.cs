﻿using System.Collections.Generic;
using Wezit.DataModels.DTO;

namespace Wezit.WebUI.Models
{
    public class TruckBookingRequestViewModel : TruckBookingRequest
    {
        public IEnumerable<ServiceProviderDTO> ServiceProviders { get; set; }
    }
}
