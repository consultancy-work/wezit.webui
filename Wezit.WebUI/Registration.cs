﻿using Microsoft.Extensions.DependencyInjection;
using Wezit.WebUI.Helpers;

namespace Wezit.WebUI
{
    public static class Registrations
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            services.AddScoped<INotificationHelper, NotificationHelper>();
            return services;
        }
    }
}
