﻿using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.WebUI.Models;

namespace Wezit.WebUI.SimpleService
{
    public class DataService : IDataService
    {
        private readonly IConfiguration _configuration;
        private readonly IRestClient _restClient;
        public string BaseUrl
        {
            get
            {
                return _configuration["CoreApiURL"];
            }
        }
        public string TrackerUrl
        {
            get
            {
                return _configuration["TrackerUrl"];
            }
        }
        public string TrackerLoginKey
        {
            get
            {
                return _configuration["TrackerLoginKey"];
            }
        }
        public string TrackerAccount
        {
            get
            {
                return _configuration["TrackerAccount"];
            }
        }

        public DataService(IConfiguration configuration,
            IRestClient restClient)
        {
            _configuration = configuration;
            _restClient = restClient;
            _restClient.BaseUrl = new System.Uri(BaseUrl);
            _restClient.UseNewtonsoftJson();
        }

        public async Task<IEnumerable<TruckOwnerDTO>> GetTruckOwners()
        {
            var request = new RestRequest("TruckOwners/GetList", DataFormat.Json);
            var truckOwners = await _restClient.GetAsync<IEnumerable<TruckOwnerDTO>>(request);
            if(truckOwners == null)
            {
                truckOwners = new List<TruckOwnerDTO>();
            }

            return truckOwners;
        }

        public async Task<IEnumerable<BandSubscriptionDTO>> GetBandSubscriptions()
        {
            var request = new RestRequest("BandSubscriptions/GetList", DataFormat.Json);
            var bandSubscriptions = await _restClient.GetAsync<IEnumerable<BandSubscriptionDTO>>(request);
            if (bandSubscriptions == null)
            {
                bandSubscriptions = new List<BandSubscriptionDTO>();
            }


            return bandSubscriptions;
        }

        public async Task<IEnumerable<CustomerDTO>> GetCustomers()
        {
            var request = new RestRequest("Customers/GetList", DataFormat.Json);
            var customers = await _restClient.GetAsync<IEnumerable<CustomerDTO>>(request);
            if (customers == null)
            {
                customers = new List<CustomerDTO>();
            }

            return customers;
        }

        public async Task<IEnumerable<TruckDTO>> GetTrucks()
        {
            var request = new RestRequest("Trucks/GetList", DataFormat.Json);
            var trucks = await _restClient.GetAsync<IEnumerable<TruckDTO>>(request);
            if (trucks == null)
            {
                trucks = new List<TruckDTO>();
            }

            return trucks;
        }
        
        public async Task<IEnumerable<EventLogStageDTO>> GetEventStages()
        {
            var request = new RestRequest("EventLogStages/GetList", DataFormat.Json);
            var stages = await _restClient.GetAsync<IEnumerable<EventLogStageDTO>>(request);
            if (stages == null)
            {
                stages = new List<EventLogStageDTO>();
            }

            return stages;
        }

        public async Task<string> GetTrackerAccessToken()
        {
            var accessToken = string.Empty;
            var unixTimestamp = DateTimeOffset.Now.ToUnixTimeSeconds();
            var signature = CreateMD5($"{CreateMD5(TrackerLoginKey)}{unixTimestamp}");

            TrackerTokenRequest tokenRequest = new TrackerTokenRequest
            {
                appid = TrackerAccount,
                time = unixTimestamp,
                signature = signature
            };
            _restClient.BaseUrl = new Uri(TrackerUrl);

            var request = new RestRequest("auth")
                   .AddJsonBody(tokenRequest);

            var response = await _restClient.ExecuteAsync<TokenResponse>(request, Method.POST);
            if (response.StatusCode == HttpStatusCode.OK && response.Data.Code == 0)
            {
                accessToken = response.Data.AccessToken;
            }
           
            return accessToken;
        }

        public async Task<TruckLocationResponse> GetCurrentTruckLocation(string imeiNumber)
        {
            var accessToken = await GetTrackerAccessToken();
            _restClient.BaseUrl = new Uri(TrackerUrl);
            var truckLocation = new TruckLocationResponse();
            var requestUrl = $"device/location?accessToken={accessToken}&imei={imeiNumber}";
            var request = new RestRequest(requestUrl, DataFormat.Json);
            truckLocation = await _restClient.GetAsync<TruckLocationResponse>(request);
            return truckLocation;
        }


        //public async Task<IEnumerable<TruckOwnerDTO>> GetTruckOwners()
        //{
        //    var request = new RestRequest("TruckOwners/GetList", DataFormat.Json);
        //    var truckOwners = await _restClient.GetAsync<IEnumerable<TruckOwnerDTO>>(request);
        //    if (truckOwners == null)
        //    {
        //        truckOwners = new List<TruckOwnerDTO>();
        //    }

        //    return truckOwners;
        //}


        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }

    }
}