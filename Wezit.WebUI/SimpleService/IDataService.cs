﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.WebUI.Models;

namespace Wezit.WebUI.SimpleService
{
    public interface IDataService
    {
        Task<IEnumerable<TruckOwnerDTO>> GetTruckOwners();

        Task<IEnumerable<BandSubscriptionDTO>> GetBandSubscriptions();

        Task<IEnumerable<TruckDTO>> GetTrucks();

        Task<IEnumerable<CustomerDTO>> GetCustomers();
        Task<IEnumerable<EventLogStageDTO>> GetEventStages();
        Task<TruckLocationResponse> GetCurrentTruckLocation(string imeiNumber);
    }
}
