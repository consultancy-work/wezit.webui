﻿using Microsoft.AspNetCore.Identity;

namespace Wezit.WebUI.Data
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
    }
}
