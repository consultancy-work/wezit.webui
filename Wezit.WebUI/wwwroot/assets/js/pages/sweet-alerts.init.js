
$(document).ready(function () {

    var toastMessage = Cookies.get('toast_message');

    if (toastMessage ) {

        var toastMessageType = Cookies.get("toast_message_type");
        var messageType;

        switch (toastMessageType) {
            case 0:
                messageType = "success";
            case 1:
                messageType = "information";
            case 2:
                messageType = "warning";
            case 3:
                messageType = "error";
            case 4:
                messageType = "error";
            case 5:
                messageType = "error";
                    
            default:
                messageType = "success"
        }

        //alert(location.hostname);
        Cookies.remove("toast_message", { path: "/", domain: location.hostname });

        
        Swal.fire({
            toast: false,
            position: 'top-end',
            backdrop: 'rgba(45, 5, 5, 0.42);',
            icon: messageType,
            title: toastMessage,
            showConfirmButton: false,
            timer: 1500
        });


    }
    



});
