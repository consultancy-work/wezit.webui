﻿using Wezit.DataModels.General;

namespace Wezit.WebUI.Helpers
{
    public interface INotificationHelper
    {
        void SendEmail(string to, string subject, string body);
        FeedbackMessage SendSms(string toPhoneNumber, string text);
    }
}