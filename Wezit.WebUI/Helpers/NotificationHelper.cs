﻿using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Net;
using System.Net.Mail;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.WebUI.Helpers
{
    public class NotificationHelper : INotificationHelper
    {

        private readonly IConfiguration _configuration;
        public NotificationHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void SendEmail(string to, string subject, string body)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress("v2vikhumbo@gmail.com");
            mail.To.Add(to);
            mail.Subject = subject;
            mail.Body = body;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new NetworkCredential("v2vikhumbo@gmail.com", "v12m81k0");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }

        public FeedbackMessage SendSms(string toPhoneNumber, string text)
        {
            // Find your Account SID and Auth Token at twilio.com/console
            // and set the environment variables. See http://twil.io/secure
            string accountSid = _configuration["TWILIO_ACCOUNT_SID"];
            string authToken = _configuration["TWILIO_AUTH_TOKEN"];

            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                body: text,
                from: new Twilio.Types.PhoneNumber("+16085239176"),
                to: new Twilio.Types.PhoneNumber(toPhoneNumber)
            );
            }
            catch (TwilioException e)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = e.Message
                };
            }
            return new FeedbackMessage
            {
                HasErrorOccured = false
            };
        }
    }
}
